import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vSelect from 'vue-select'
import VueTheMask from 'vue-the-mask'
import Multiselect from 'vue-multiselect'
import VueSlickCarousel from 'vue-slick-carousel'
import VueScrollactive from 'vue-scrollactive';
import Vuelidate from 'vuelidate'
import 'vue-slick-carousel/dist/vue-slick-carousel.css'
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'
import store from './store'
import { VueStars } from "vue-stars"
import Notifications from 'vue-notification'
Vue.component("vue-stars", VueStars)
Vue.use(Notifications)
Vue.use(Vuelidate)
Vue.use(VueTheMask)

Vue.use(VueScrollactive);
Vue.component('multiselect', Multiselect)
Vue.component('VueSlickCarousel', VueSlickCarousel);

Vue.config.productionTip = false

Vue.component('v-select', vSelect)
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
